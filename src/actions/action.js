import ACTION from "./actionTypes"

export const setStudentDetails = (studentDetails) => {
    return {
        type: ACTION.ADD_STUDENT_DETAILS,
        payload: studentDetails
    }
};

export const setStartDate = (startDate) => {
    return {
        type: ACTION.SET_START_DATE,
        payload: startDate
    }
}

export const setEndDate = (endDate) => {
    return {
        type: ACTION.SET_END_DATE,
        payload: endDate
    }
}

export const setStatus = (status) => {
    return {
        type: ACTION.SET_STATUS,
        payload: status
    }
}

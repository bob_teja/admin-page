import React, { Component } from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import '../css/App.css'

import {setStudentDetails, setStartDate, setEndDate, setStatus} from "../actions/action"
import StudentListItem from './StudentListItem';

class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      data : [
        {
          id:1,
          emailId:"asdg",
          firstName:"teja",
          lastName:"sai",
          mobileNumber:"113",
          collegeName:"vnr",
          photoLink:"https://images.pexels.com/photos/46710/pexels-photo-46710.jpeg?cs=srgb&dl=beach-sand-summer-46710.jpg&fm=jpg"
        },
        {
         id:2,
         emailId:"asdg",
         firstName:"daljith",
         lastName:"singh",
         mobileNumber:"112",
         collegeName:"vignan",
         photoLink:"https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg?cs=srgb&dl=beach-exotic-holiday-248797.jpg&fm=jpg"
        },
        {
         id:3,
         emailId:"asdg",
         firstName:"prasenjeet",
         lastName:"paul",
         mobileNumber:"111",
         collegeName:"vjit",
         photoLink:"https://images.pexels.com/photos/155246/pexels-photo-155246.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
        }
      ]
    };
  }

  // static getDerivedStateFromProps(props, state=null) { 
  //   console.log("Started Loading data");
  //   props.setStudentDetails(state.data);
  //   return state;
  // }


  displayDate = (event) => {
    event.preventDefault();
    var startDate = document.getElementById("startDate").value;
    var endDate = document.getElementById("endDate").value;
    var status = document.getElementById("status").value;
    if(startDate.length===0){alert("Please Select Start date");return;}
    if(endDate.length===0){alert("Please Select End  date");return;}
    if(+new Date(startDate).getDate() > +new Date(endDate).getDate()){alert("Improper start date and end date selected");return;}
  
    
    this.props.setStartDate(startDate);
    this.props.setEndDate(endDate);
    this.props.setStatus(status);

    //TODO REPLACE FETCH API CALL HERE
    this.props.setStudentDetails(this.state.data);
  }
  render() {
    const {studentDetails} = this.props.studentDetailsReducer;
    return (
      <div className="App">
        <form onSubmit={this.displayDate}>
          Start Date<input type="date" id="startDate" ></input>
          End Date<input type="date" id="endDate" ></input>
          <select id="status">
            <option value="All">All</option>
            <option value="Approved">Approved</option>
            <option value="Rejected">Rejected</option>
          </select>
          <input type="submit" value="Get Details"/>
        </form>
          {
             (studentDetails !== null)
             ? studentDetails.map((eachStudent)=>
                  <StudentListItem key={Math.floor(Math.random()*1000000)} student={eachStudent}/>)
             : <b>Submit the form to load data</b>
          }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    studentDetailsReducer: state.studentDetailsReducer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
      setStudentDetails : setStudentDetails,
      setStartDate : setStartDate,
      setEndDate : setEndDate,
      setStatus : setStatus
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);


//onChange={this.displayDate}
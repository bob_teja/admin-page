import React, { Component } from 'react';
import '../css/StudentListItem.css'

class StudentListItem extends Component {

    viewResume = (event) => {
        event.preventDefault();
        console.log("View Resume "+ this.props.student.firstName+"  id: "+this.props.student.id); 
    }

    viewPhoto = (event) => {
        event.preventDefault();
        console.log("View Photo "+ this.props.student.firstName+"  id: "+this.props.student.id); 
    }

    approveStudent = (event) => {
        event.preventDefault();
        console.log("Approve Student "+ this.props.student.firstName+"  id: "+this.props.student.id); 
    }

    rejectStudent = (event) => {
        event.preventDefault();
        console.log("Reject Student "+ this.props.student.firstName+"  id: "+this.props.student.id); 
    }

    render() {
        const {student} = this.props;
        console.log(student);
        return (
            <div>
                EMAIL ID: {student.emailId+" "}
                First Name: {student.firstName+" "}
                Last Name: {student.lastName+" "}
                Mobile Number: {student.mobileNumber+" "}
                College: {student.collegeName+" "}
                Id: {student.id+ " "} 
                <img src={student.photoLink} alt={student.firstName}/>
                <button onClick={this.viewResume}>View Resume</button>
                <button onClick={this.viewPhoto}>View Photo</button>
                <button onClick={this.approveStudent}>Approve</button>
                <button onClick={this.rejectStudent}>Reject</button>
            </div>
        )
    }
}

export default StudentListItem;
import {combineReducers} from 'redux';
import StudentDetailsReducer from  "./studentDetailsReducer"

const allReducers = combineReducers({
    studentDetailsReducer: StudentDetailsReducer
});

export default allReducers;
import ACTION from "../actions/actionTypes"
import initialState from "../store/initialState";

export default function(state = initialState,action) {
    switch (action.type) {
        case ACTION.ADD_STUDENT_DETAILS : {
            return {
                ...state,
                studentDetails: action.payload
            }
        }
        case ACTION.SET_START_DATE : {
            console.log("Start date set")
            return {
                ...state,
                startDate : action.payload
            }
        }
        case ACTION.SET_END_DATE : {
            console.log("End date set")
            return {
                ...state,
                endDate : action.payload
            }
        }
        case ACTION.SET_STATUS : {
            console.log("status set")
            console.log(action.payload)
            return {
                ...state,
                status : action.payload
            }
        }
        default: return state;
    }
}
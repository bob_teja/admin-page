const initialState = {
    studentDetails : null,
    startDate : null,
    endDate : null,
    status : "All"
};

export default initialState;
/*
[
        {
          id:1,
          name:"teja"
        },
        {
         id:2,
         name:"daljith"
        },
        {
         id:3,
         name:"prasenjeet"
        }
      ]
*/